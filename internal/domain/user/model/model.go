package model

import (
	"fmt"
	"github.com/semichkin-gopkg/uuid"
	"gitlab.collabox.dev/go/errors"
	"golang.org/x/crypto/bcrypt"
	"main/internal/adapter/repository"
	"main/internal/domain"
)

type UserModel struct {
	userRepository repository.UserRepository
	tokenService   domain.TokenService
}

func NewUserModel(
	userRepository repository.UserRepository,
	tokenService domain.TokenService,
) *UserModel {
	return &UserModel{
		userRepository: userRepository,
		tokenService:   tokenService,
	}
}

func (m *UserModel) SignIn(data domain.UserLoginData) (domain.UserInfo, error) {
	if err := data.Validate(); err != nil {
		return domain.UserInfo{}, err
	}

	user, err := m.userRepository.GetByLogin(data.Login)
	if err != nil {
		return domain.UserInfo{}, err
	}

	if err := bcrypt.CompareHashAndPassword(
		[]byte(user.Password), []byte(data.Password)); err != nil {
		return domain.UserInfo{}, errors.ErrBadRequest.WithMessage("incorrect password")
	}

	if user.Token == "" {
		token, err := m.tokenService.Generate(domain.TokenPayload{
			ID:    user.ID,
			Login: user.Login,
			Role:  user.Role,
		})
		if err != nil {
			return domain.UserInfo{}, err
		}

		if err := m.userRepository.SetToken(user.ID, token); err != nil {
			return domain.UserInfo{}, err
		}

		user.Token = token
	}

	return domain.UserInfo{
		Login: user.Login,
		Role:  user.Role,
		Token: user.Token,
	}, nil
}

func (m *UserModel) SignUp(data domain.UserRegisterData) (domain.UserInfo, error) {
	if err := data.Validate(); err != nil {
		return domain.UserInfo{}, err
	}

	_, err := m.userRepository.GetByLogin(data.Login)
	if err == nil {
		return domain.UserInfo{}, errors.ErrBadRequest.WithMessage(fmt.Sprintf("user with login already exists: %s", data.Login))
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(data.Password), 14)
	if err != nil {
		return domain.UserInfo{}, err
	}

	data.Password = string(passwordHash)

	id, err := m.userRepository.Save(data)
	if err != nil {
		return domain.UserInfo{}, err
	}

	token, err := m.tokenService.Generate(domain.TokenPayload{
		ID:    id,
		Login: data.Login,
		Role:  data.Role,
	})
	if err != nil {
		return domain.UserInfo{}, err
	}

	if err := m.userRepository.SetToken(id, token); err != nil {
		return domain.UserInfo{}, err
	}

	return domain.UserInfo{
		Login: data.Login,
		Role:  data.Role,
		Token: token,
	}, nil
}

func (m *UserModel) SignOut(id uuid.UUID) error {
	return m.userRepository.DeleteToken(id)
}

func (m *UserModel) GetMe(id uuid.UUID) (domain.UserInfo, error) {
	user, err := m.userRepository.Get(id)
	if err != nil {
		return domain.UserInfo{}, err
	}

	return domain.UserInfo{
		Login: user.Login,
		Role:  user.Role,
		Token: user.Token,
	}, nil
}
