package service

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/semichkin-gopkg/converter"
	"main/internal/domain"
)

type JwtTokenService struct {
	SigningKey string
}

func NewJwtTokenService(signingKey string) *JwtTokenService {
	return &JwtTokenService{
		SigningKey: signingKey,
	}
}

func (s *JwtTokenService) Generate(payload domain.TokenPayload) (domain.Token, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":    payload.ID.String(),
		"login": payload.Login,
		"role":  payload.Role,
	})

	tokenString, err := token.SignedString([]byte(s.SigningKey))

	return domain.Token(tokenString), err
}

func (s *JwtTokenService) Verify(token domain.Token) (domain.TokenPayload, error) {
	t, err := jwt.Parse(string(token), func(token *jwt.Token) (interface{}, error) {
		return []byte(s.SigningKey), nil
	})
	if err != nil {
		return domain.TokenPayload{}, err
	}

	payload, err := converter.Convert[domain.TokenPayload](t.Claims)
	if err != nil {
		return domain.TokenPayload{}, err
	}

	return payload, err
}
