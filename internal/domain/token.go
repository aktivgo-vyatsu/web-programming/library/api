package domain

import "github.com/semichkin-gopkg/uuid"

type (
	Token string

	TokenPayload struct {
		ID    uuid.UUID `json:"id"`
		Login string    `json:"login"`
		Role  UserRole  `json:"role"`
	}
)

type (
	TokenService interface {
		Generate(TokenPayload) (Token, error)
		Verify(Token) (TokenPayload, error)
	}
)
