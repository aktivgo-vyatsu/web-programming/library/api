package domain

import (
	"github.com/semichkin-gopkg/uuid"
	"gitlab.collabox.dev/go/errors"
	"time"
)

type (
	Book struct {
		ID          uuid.UUID `json:"id" db:"id"`
		Title       string    `json:"title" db:"title"`
		Author      string    `json:"author" db:"author"`
		Genre       string    `json:"genre" db:"genre"`
		Price       float64   `json:"price" db:"price"`
		Description string    `json:"description" db:"description"`
		Image       string    `json:"image" db:"image"`
		CreatedAt   time.Time `json:"created_at" db:"created_at"`
	}

	BookCreateData struct {
		Title       string  `json:"title"`
		Author      string  `json:"author"`
		Genre       string  `json:"genre"`
		Price       float64 `json:"price"`
		Description string  `json:"description"`
		Image       string  `json:"image"`
	}

	BookUpdateData struct {
		Title       string  `json:"title"`
		Author      string  `json:"author"`
		Genre       string  `json:"genre"`
		Price       float64 `json:"price"`
		Description string  `json:"description"`
		Image       string  `json:"image"`
	}
)

func (b *BookCreateData) Validate() error {
	if b.Title == "" {
		return errors.ErrBadRequest.WithMessage("title is empty")
	}

	if b.Author == "" {
		return errors.ErrBadRequest.WithMessage("author is empty")
	}

	if b.Genre == "" {
		return errors.ErrBadRequest.WithMessage("genre is empty")
	}

	if b.Price == 0 {
		return errors.ErrBadRequest.WithMessage("price is empty")
	}

	if b.Description == "" {
		return errors.ErrBadRequest.WithMessage("description is empty")
	}

	if b.Image == "" {
		return errors.ErrBadRequest.WithMessage("image is empty")
	}

	return nil
}

func (b *BookUpdateData) Validate() error {
	if b.Title == "" && b.Author == "" && b.Price == 0 && b.Description == "" && b.Image == "" {
		return errors.ErrBadRequest.WithMessage("update request is empty")
	}

	return nil
}

type (
	BookModel interface {
		Get(uuid.UUID) (Book, error)
		GetList(int, int) ([]Book, error)
		Create(BookCreateData) (uuid.UUID, error)
		Update(uuid.UUID, BookUpdateData) error
		Delete(uuid.UUID) error
	}
)
