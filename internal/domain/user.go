package domain

import (
	"errors"
	"github.com/semichkin-gopkg/uuid"
	"time"
)

var (
	UserNotFoundErr = errors.New("user npt found")
)

type (
	User struct {
		ID        uuid.UUID `json:"id" db:"id"`
		Login     string    `json:"login" db:"login"`
		Password  string    `json:"password" db:"password"`
		Role      UserRole  `json:"role" db:"role"`
		Token     Token     `json:"token,omitempty" db:"token"`
		CreatedAt time.Time `json:"created_at" db:"created_at"`
	}

	UserInfo struct {
		Login string   `json:"login"`
		Role  UserRole `json:"role"`
		Token Token    `json:"token,omitempty" db:"token"`
	}

	UserLoginData struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}

	UserRegisterData struct {
		Login    string   `json:"login"`
		Password string   `json:"password"`
		Role     UserRole `json:"role"`
	}
)

type UserRole = string

func (u *UserLoginData) Validate() error {
	if u.Login == "" {
		return errors.New("login is empty")
	}

	if u.Password == "" {
		return errors.New("password is empty")
	}

	return nil
}

func (u *UserRegisterData) Validate() error {
	if u.Login == "" {
		return errors.New("login is empty")
	}

	if u.Password == "" {
		return errors.New("password is empty")
	}

	return nil
}

type (
	UserModel interface {
		SignIn(UserLoginData) (UserInfo, error)
		SignUp(UserRegisterData) (UserInfo, error)
		SignOut(uuid.UUID) error
		GetMe(uuid.UUID) (UserInfo, error)
	}
)
