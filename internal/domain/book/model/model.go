package model

import (
	"main/internal/adapter/repository"
	"main/internal/domain"

	"github.com/semichkin-gopkg/uuid"
)

type BookModel struct {
	bookRepository repository.BookRepository
}

func NewBookModel(bookRepository repository.BookRepository) *BookModel {
	return &BookModel{bookRepository: bookRepository}
}

func (m *BookModel) Get(id uuid.UUID) (domain.Book, error) {
	return m.bookRepository.Get(id)
}

func (m *BookModel) GetList(offset int, limit int) ([]domain.Book, error) {
	return m.bookRepository.GetList(offset, limit)
}

func (m *BookModel) Create(data domain.BookCreateData) (uuid.UUID, error) {
	if err := data.Validate(); err != nil {
		return uuid.Nil(), err
	}
	return m.bookRepository.Save(data)
}

func (m *BookModel) Update(id uuid.UUID, data domain.BookUpdateData) error {
	if err := data.Validate(); err != nil {
		return err
	}
	return m.bookRepository.Update(id, data)
}

func (m *BookModel) Delete(id uuid.UUID) error {
	return m.bookRepository.Delete(id)
}
