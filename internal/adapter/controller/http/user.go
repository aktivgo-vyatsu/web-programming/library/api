package http

import "net/http"

type UserController interface {
	SignIn(http.ResponseWriter, *http.Request)
	SignUp(http.ResponseWriter, *http.Request)
	GetMe(http.ResponseWriter, *http.Request)
	SignOut(http.ResponseWriter, *http.Request)
}
