package http

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.collabox.dev/go/errors"
	"io"
	"main/pkg/logging"
	"net/http"
)

type BaseHttpController struct {
	Logger logging.Logger
}

func NewBaseHttpController(logger logging.Logger) *BaseHttpController {
	return &BaseHttpController{
		Logger: logger,
	}
}

func (c *BaseHttpController) GetUrlVariable(r *http.Request, variable string) (string, error) {
	value, ok := mux.Vars(r)[variable]
	if !ok {
		return "", errors.ErrNotFound.WithMessage(fmt.Sprintf("url variable not found: %s", variable))
	}

	return value, nil
}

func (c *BaseHttpController) GetHeaderValue(r *http.Request, key string) (string, error) {
	value := r.Header.Get(key)
	if value == "" {
		return "", errors.ErrBadRequest.WithMessage(fmt.Sprintf("header not found: %s", key))
	}

	return value, nil
}

func (c *BaseHttpController) GetQueryParameter(r *http.Request, parameter string) (string, error) {
	value := r.URL.Query().Get(parameter)
	if value == "" {
		return "", errors.ErrNotFound.WithMessage(fmt.Sprintf("query parameter not found: %s", parameter))
	}

	return value, nil
}

func (c *BaseHttpController) ParseRequestBody(r *http.Request, data any) error {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		c.Logger.Error(1, fmt.Sprintf("app error: %s", err.Error()), nil)
		return errors.ErrInternal.WithMessage(fmt.Sprintf("body reading error: %s", err.Error()))
	}

	if err = json.Unmarshal(body, &data); err != nil {
		return errors.ErrBadRequest.WithMessage(fmt.Sprintf("invalid request body: %s", err.Error()))
	}

	return nil
}

func (c *BaseHttpController) Respond(w http.ResponseWriter, status int, data any) {
	w.Header().Add("Content-Type", "application/json")

	if js, err := json.Marshal(data); err == nil {
		w.WriteHeader(status)
		_, _ = w.Write(js)
		return
	}

	w.WriteHeader(http.StatusInternalServerError)
	if js, err := json.Marshal(map[string]any{
		"description": "internal server error",
	}); err == nil {
		_, _ = w.Write(js)
	}
}

func (c *BaseHttpController) RespondWithError(w http.ResponseWriter, err error) {
	w.Header().Add("Content-Type", "application/json")

	if errors.Is(err, errors.ErrForbidden) {
		c.Respond(w, http.StatusForbidden, err)
		return
	}
	if errors.Is(err, errors.ErrNotFound) {
		c.Respond(w, http.StatusNotFound, err)
		return
	}
	if errors.Is(err, errors.ErrBadRequest) {
		c.Respond(w, http.StatusBadRequest, err)
		return
	}
	if errors.Is(err, errors.ErrUnauthorized) {
		c.Respond(w, http.StatusUnauthorized, err)
		return
	}

	c.Logger.Error(1, fmt.Sprintf("app error: %s", err.Error()), nil)
	c.Respond(w, http.StatusInternalServerError, map[string]any{
		"description": "internal server error",
	})
}
