package user

import (
	"github.com/semichkin-gopkg/uuid"
	httpController "main/internal/adapter/controller/http"
	"main/internal/domain"
	"main/pkg/logging"
	"net/http"
	"time"
)

type HttpUserController struct {
	*httpController.BaseHttpController

	userModel domain.UserModel
}

func NewHttpUserController(
	logger logging.Logger,
	userModel domain.UserModel,
) *HttpUserController {
	return &HttpUserController{
		BaseHttpController: httpController.NewBaseHttpController(logger),
		userModel:          userModel,
	}
}

func (c *HttpUserController) SignIn(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		var data domain.UserLoginData
		if err := c.ParseRequestBody(request, &data); err != nil {
			return err
		}

		userInfo, err := c.userModel.SignIn(data)
		if err != nil {
			return err
		}

		c.setAuthCookie(writer, userInfo.Token)

		c.Respond(writer, http.StatusOK, map[string]any{
			"login": userInfo.Login,
			"role":  userInfo.Role,
		})

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpUserController) SignUp(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		var data domain.UserRegisterData
		if err := c.ParseRequestBody(request, &data); err != nil {
			return err
		}

		userInfo, err := c.userModel.SignUp(data)
		if err != nil {
			return err
		}

		c.setAuthCookie(writer, userInfo.Token)

		c.Respond(writer, http.StatusCreated, map[string]any{
			"login": userInfo.Login,
			"role":  userInfo.Role,
		})

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpUserController) SignOut(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		userID, err := c.GetHeaderValue(request, "user_id")
		if err != nil {
			return err
		}

		id, err := uuid.Parse(userID)
		if err != nil {
			return err
		}

		if err := c.userModel.SignOut(id); err != nil {
			return err
		}

		c.unsetAuthCookie(writer)

		c.Respond(writer, http.StatusNoContent, nil)

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpUserController) GetMe(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		userID, err := c.GetHeaderValue(request, "user_id")
		if err != nil {
			return err
		}

		id, err := uuid.Parse(userID)
		if err != nil {
			return err
		}

		userInfo, err := c.userModel.GetMe(id)
		if err != nil {
			return err
		}

		c.setAuthCookie(writer, userInfo.Token)

		c.Respond(writer, http.StatusOK, map[string]any{
			"login": userInfo.Login,
			"role":  userInfo.Role,
		})

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpUserController) setAuthCookie(writer http.ResponseWriter, token domain.Token) {
	cookie := http.Cookie{
		Name:     "auth-token",
		Value:    string(token),
		Path:     "/",
		Domain:   "library.loc",
		HttpOnly: true,
	}
	http.SetCookie(writer, &cookie)
}

func (c *HttpUserController) unsetAuthCookie(writer http.ResponseWriter) {
	cookie := http.Cookie{
		Name:     "auth-token",
		Value:    "",
		Path:     "/",
		Domain:   "library.loc",
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
	}
	http.SetCookie(writer, &cookie)
}
