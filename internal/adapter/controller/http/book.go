package http

import "net/http"

type BookController interface {
	Get(http.ResponseWriter, *http.Request)
	GetList(http.ResponseWriter, *http.Request)
	Create(http.ResponseWriter, *http.Request)
	Update(http.ResponseWriter, *http.Request)
	Delete(http.ResponseWriter, *http.Request)
}
