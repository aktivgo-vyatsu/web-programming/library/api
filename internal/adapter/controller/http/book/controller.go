package book

import (
	"github.com/semichkin-gopkg/uuid"
	"gitlab.collabox.dev/go/errors"
	httpController "main/internal/adapter/controller/http"
	"main/internal/domain"
	"main/pkg/logging"
	"net/http"
	"strconv"
)

type HttpBookController struct {
	*httpController.BaseHttpController

	userModel domain.UserModel
	bookModel domain.BookModel
}

func NewHttpBookController(
	logger logging.Logger,
	userModel domain.UserModel,
	bookModel domain.BookModel,
) *HttpBookController {
	return &HttpBookController{
		BaseHttpController: httpController.NewBaseHttpController(logger),
		userModel:          userModel,
		bookModel:          bookModel,
	}
}

func (c *HttpBookController) Get(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		id, err := c.getBookID(request)
		if err != nil {
			return err
		}

		book, err := c.bookModel.Get(id)
		if err != nil {
			return err
		}

		c.Respond(writer, http.StatusOK, book)

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpBookController) GetList(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		offset, limit, err := c.getPagination(request)
		if err != nil {
			return err
		}

		book, err := c.bookModel.GetList(offset, limit)
		if err != nil {
			return err
		}

		c.Respond(writer, http.StatusOK, book)

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpBookController) Create(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		var data domain.BookCreateData
		if err := c.ParseRequestBody(request, &data); err != nil {
			return err
		}

		id, err := c.bookModel.Create(data)
		if err != nil {
			return err
		}

		c.Respond(writer, http.StatusCreated, map[string]any{
			"id": id,
		})

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpBookController) Update(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		id, err := c.getBookID(request)
		if err != nil {
			return err
		}

		var data domain.BookUpdateData
		if err := c.ParseRequestBody(request, &data); err != nil {
			return err
		}

		if err := c.bookModel.Update(id, data); err != nil {
			return err
		}

		c.Respond(writer, http.StatusOK, nil)

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpBookController) Delete(writer http.ResponseWriter, request *http.Request) {
	if err := func() error {
		id, err := c.getBookID(request)
		if err != nil {
			return err
		}

		if err := c.bookModel.Delete(id); err != nil {
			return err
		}

		c.Respond(writer, http.StatusNoContent, nil)

		return nil
	}(); err != nil {
		c.RespondWithError(writer, err)
	}
}

func (c *HttpBookController) getBookID(request *http.Request) (uuid.UUID, error) {
	id, err := c.GetUrlVariable(request, "id")
	if err != nil {
		return uuid.Nil(), err
	}

	resp, err := uuid.Parse(id)
	if err != nil {
		return uuid.Nil(), errors.ErrBadRequest.WithMessage(err.Error())
	}

	return resp, nil
}

func (c *HttpBookController) getPagination(request *http.Request) (int, int, error) {
	defaultOffset := 0
	defaultLimit := 10

	offset, err := c.GetQueryParameter(request, "offset")
	if err != nil {
		return defaultOffset, defaultLimit, nil
	}

	offsetValue, err := strconv.Atoi(offset)
	if err != nil {
		return defaultOffset, defaultLimit, err
	}

	limit, err := c.GetQueryParameter(request, "limit")
	if err != nil {
		return offsetValue, defaultLimit, nil
	}

	limitValue, err := strconv.Atoi(limit)
	if err != nil {
		return offsetValue, defaultLimit, err
	}

	return offsetValue, limitValue, nil
}
