package middleware

import "net/http"

type AuthMiddleware interface {
	Authentication(http.Handler) http.Handler
	AdminAuthorization(http.Handler) http.Handler
}
