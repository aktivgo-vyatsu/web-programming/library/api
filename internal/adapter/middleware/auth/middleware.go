package auth

import (
	"fmt"
	"gitlab.collabox.dev/go/errors"
	httpController "main/internal/adapter/controller/http"
	"main/internal/domain"
	"main/pkg/logging"
	"net/http"
)

type AuthMiddleware struct {
	*httpController.BaseHttpController

	tokenService domain.TokenService
}

func NewAuthMiddleware(
	logger logging.Logger,
	tokenService domain.TokenService,
) *AuthMiddleware {
	return &AuthMiddleware{
		BaseHttpController: httpController.NewBaseHttpController(logger),
		tokenService:       tokenService,
	}
}

func (m *AuthMiddleware) Authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := func() error {
			token, err := m.getAuthCookie(r)
			if err != nil {
				return errors.ErrUnauthorized.WithMessage("missing auth cookie")
			}

			payload, err := m.tokenService.Verify(token)
			if err != nil {
				return errors.ErrUnauthorized.WithMessage(fmt.Sprintf("error verifying token: %s", err.Error()))
			}

			r.Header.Set("user_id", string(payload.ID))
			r.Header.Set("user_login", payload.Login)
			r.Header.Set("user_role", payload.Role)

			next.ServeHTTP(w, r)

			return nil
		}(); err != nil {
			m.RespondWithError(w, err)
		}
	})
}

func (m *AuthMiddleware) AdminAuthorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := func() error {
			role, err := m.GetHeaderValue(r, "user_role")
			if err != nil {
				return err
			}

			if role != "administrator" {
				return errors.ErrForbidden.WithMessage("this action is only available to the administrator")
			}

			next.ServeHTTP(w, r)

			return nil
		}(); err != nil {
			m.RespondWithError(w, err)
		}
	})
}

func (m *AuthMiddleware) getAuthCookie(request *http.Request) (domain.Token, error) {
	cookie, err := request.Cookie("auth-token")
	if err != nil {
		return "", errors.ErrUnauthorized
	}

	return domain.Token(cookie.Value), nil
}
