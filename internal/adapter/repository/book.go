package repository

import (
	"github.com/semichkin-gopkg/uuid"
	"main/internal/domain"
)

type BookRepository interface {
	Get(uuid.UUID) (domain.Book, error)
	GetList(int, int) ([]domain.Book, error)
	Save(domain.BookCreateData) (uuid.UUID, error)
	Update(uuid.UUID, domain.BookUpdateData) error
	Delete(uuid.UUID) error
}
