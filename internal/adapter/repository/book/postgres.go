package book

import (
	"database/sql"
	"fmt"
	"gitlab.collabox.dev/go/errors"
	"log"
	"main/internal/domain"
	"strconv"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/semichkin-gopkg/uuid"
)

type PostgresBookRepository struct {
	client *sqlx.DB
}

func NewPostgresBookRepository(client *sqlx.DB) *PostgresBookRepository {
	return &PostgresBookRepository{client: client}
}

func (r *PostgresBookRepository) Get(id uuid.UUID) (domain.Book, error) {
	query := `
		SELECT * FROM books
		WHERE id = $1
	`

	var book domain.Book
	if err := r.client.Get(&book, query, id); err != nil {
		if err == sql.ErrNoRows {
			return domain.Book{}, errors.ErrNotFound.WithMessage(fmt.Sprintf("book not found: %s", id))
		}

		return domain.Book{}, err
	}

	return book, nil
}

func (r *PostgresBookRepository) GetList(offset int, limit int) ([]domain.Book, error) {
	query := `
		SELECT * FROM books
		ORDER BY created_at
		OFFSET $1 LIMIT $2
	`

	books := make([]domain.Book, 0)
	if err := r.client.Select(&books, query, offset, limit); err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.ErrNotFound.WithMessage("books not found")
		}

		return nil, err
	}

	return books, nil
}

func (r *PostgresBookRepository) Save(data domain.BookCreateData) (uuid.UUID, error) {
	id := uuid.New()

	query := `
		INSERT INTO books (id, title, author, genre, price, description, image)
			VALUES ($1, $2, $3, $4, $5, $6, $7)
	`

	args := []any{
		id, data.Title, data.Author, data.Genre, data.Price, data.Description, data.Image,
	}

	_, err := r.client.Exec(query, args...)
	if err != nil {
		return uuid.Nil(), err
	}

	return id, nil
}

func (r *PostgresBookRepository) Update(id uuid.UUID, data domain.BookUpdateData) error {
	query := `
		UPDATE books SET __fields__
		WHERE id = $__id__
	`

	fields := ""
	fieldsCount := 0
	var args []any

	if data.Title != "" {
		fieldsCount++
		fields += `title=$` + strconv.Itoa(fieldsCount)
		args = append(args, data.Title)
	}

	if data.Author != "" {
		fieldsCount++
		fields += `, author=$` + strconv.Itoa(fieldsCount)
		args = append(args, data.Author)
	}

	if data.Genre != "" {
		fieldsCount++
		fields += `, genre=$` + strconv.Itoa(fieldsCount)
		args = append(args, data.Genre)
	}

	if data.Price != 0 {
		fieldsCount++
		fields += `, price=$` + strconv.Itoa(fieldsCount)
		args = append(args, data.Price)
	}

	if data.Description != "" {
		fieldsCount++
		fields += `, description=$` + strconv.Itoa(fieldsCount)
		args = append(args, data.Description)
	}

	if data.Image != "" {
		fieldsCount++
		fields += `, image=$` + strconv.Itoa(fieldsCount)
		args = append(args, data.Image)
	}

	query = strings.Replace(query, "__fields__", fields, -1)
	query = strings.Replace(query, "__id__", strconv.Itoa(fieldsCount+1), -1)
	args = append(args, id)

	log.Println(query, args)

	_, err := r.client.Exec(query, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *PostgresBookRepository) Delete(id uuid.UUID) error {
	query := `
		DELETE FROM books
		WHERE id = $1
	`

	_, err := r.client.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
