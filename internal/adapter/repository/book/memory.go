package book

import (
	"fmt"
	"github.com/semichkin-gopkg/uuid"
	"gitlab.collabox.dev/go/errors"
	"main/internal/domain"
)

type MemoryBookRepository struct {
	books map[uuid.UUID]domain.Book
}

func NewMemoryBookRepository() *MemoryBookRepository {
	return &MemoryBookRepository{
		books: make(map[uuid.UUID]domain.Book),
	}
}

func (r *MemoryBookRepository) Get(id uuid.UUID) (domain.Book, error) {
	book, ok := r.books[id]
	if !ok {
		return domain.Book{}, errors.ErrNotFound.WithMessage(fmt.Sprintf("book not found: %s", id))
	}

	return book, nil
}

func (r *MemoryBookRepository) GetList(offset int, limit int) ([]domain.Book, error) {
	books := make([]domain.Book, 0)
	for _, book := range r.books {
		books = append(books, book)
	}
	return books, nil
}

func (r *MemoryBookRepository) Save(data domain.BookCreateData) (uuid.UUID, error) {
	id := uuid.New()
	r.books[id] = domain.Book{
		ID:          id,
		Title:       data.Title,
		Author:      data.Author,
		Price:       data.Price,
		Description: data.Description,
		Image:       data.Image,
	}
	return id, nil
}

func (r *MemoryBookRepository) Update(id uuid.UUID, data domain.BookUpdateData) error {
	book, err := r.Get(id)
	if err != nil {
		return err
	}

	if data.Title != "" {
		book.Title = data.Title
	}

	if data.Author != "" {
		book.Author = data.Author
	}

	if data.Genre != "" {
		book.Genre = data.Genre
	}

	if data.Price != 0 {
		book.Price = data.Price
	}

	if data.Description != "" {
		book.Description = data.Description
	}

	if data.Image != "" {
		book.Image = data.Image
	}

	r.books[id] = book

	return nil
}

func (r *MemoryBookRepository) Delete(id uuid.UUID) error {
	delete(r.books, id)
	return nil
}
