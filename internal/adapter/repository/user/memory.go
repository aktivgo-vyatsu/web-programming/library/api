package user

import (
	"github.com/semichkin-gopkg/uuid"
	"main/internal/domain"
)

type MemoryUserRepository struct {
	users map[uuid.UUID]domain.User
}

func NewMemoryUserRepository() *MemoryUserRepository {
	return &MemoryUserRepository{
		users: make(map[uuid.UUID]domain.User),
	}
}

func (r *MemoryUserRepository) Get(id uuid.UUID) (domain.User, error) {
	user, ok := r.users[id]
	if !ok {
		return domain.User{}, domain.UserNotFoundErr
	}

	return user, nil
}

func (r *MemoryUserRepository) GetByLogin(login string) (domain.User, error) {
	for _, user := range r.users {
		if user.Login == login {
			return user, nil
		}
	}

	return domain.User{}, domain.UserNotFoundErr
}

func (r *MemoryUserRepository) GetByLoginData(data domain.UserLoginData) (domain.User, error) {
	for _, user := range r.users {
		if user.Login == data.Login && user.Password == user.Password {
			return user, nil
		}
	}

	return domain.User{}, domain.UserNotFoundErr
}

func (r *MemoryUserRepository) Save(data domain.UserRegisterData) (uuid.UUID, error) {
	id := uuid.New()

	user := domain.User{
		ID:       id,
		Login:    data.Login,
		Password: data.Password,
		Role:     data.Role,
	}

	r.users[id] = user

	return id, nil
}

func (r *MemoryUserRepository) GetToken(id uuid.UUID) (domain.Token, error) {
	user, err := r.Get(id)
	if err != nil {
		return "", domain.UserNotFoundErr
	}

	return user.Token, nil
}

func (r *MemoryUserRepository) SetToken(id uuid.UUID, token domain.Token) error {
	user, err := r.Get(id)
	if err != nil {
		return domain.UserNotFoundErr
	}

	user.Token = token
	r.users[id] = user

	return nil
}

func (r *MemoryUserRepository) DeleteToken(id uuid.UUID) error {
	user, err := r.Get(id)
	if err != nil {
		return domain.UserNotFoundErr
	}

	user.Token = ""
	r.users[id] = user

	return nil
}
