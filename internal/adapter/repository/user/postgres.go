package user

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/semichkin-gopkg/uuid"
	"gitlab.collabox.dev/go/errors"
	"main/internal/domain"
)

type PostgresUserRepository struct {
	client *sqlx.DB
}

func NewPostgresUserRepository(client *sqlx.DB) *PostgresUserRepository {
	return &PostgresUserRepository{client: client}
}

func (r *PostgresUserRepository) Get(id uuid.UUID) (domain.User, error) {
	query := `
		SELECT * FROM users
		WHERE id = $1
	`

	var user domain.User
	if err := r.client.Get(&user, query, id); err != nil {
		if err == sql.ErrNoRows {
			return domain.User{}, errors.ErrNotFound.WithMessage(fmt.Sprintf("user not found: %s", id))
		}

		return domain.User{}, err
	}

	return user, nil
}

func (r *PostgresUserRepository) GetByLogin(login string) (domain.User, error) {
	query := `
		SELECT * FROM users
		WHERE login = $1
	`

	var user domain.User
	if err := r.client.Get(&user, query, login); err != nil {
		if err == sql.ErrNoRows {
			return domain.User{}, errors.ErrNotFound.WithMessage(
				fmt.Sprintf("user not found: %s", login),
			)
		}

		return domain.User{}, err
	}

	return user, nil
}

func (r *PostgresUserRepository) GetByLoginData(data domain.UserLoginData) (domain.User, error) {
	query := `
		SELECT * FROM users
		WHERE login = $1 AND password = $2
	`

	var user domain.User
	if err := r.client.Get(&user, query, data.Login, data.Password); err != nil {
		if err == sql.ErrNoRows {
			return domain.User{}, errors.ErrNotFound.WithMessage(
				fmt.Sprintf("user not found: %s %s", data.Login, data.Password),
			)
		}

		return domain.User{}, err
	}

	return user, nil
}

func (r *PostgresUserRepository) Save(data domain.UserRegisterData) (uuid.UUID, error) {
	id := uuid.New()

	query := `
		INSERT INTO users (id, login, password, role)
			VALUES ($1, $2, $3, $4)
	`

	args := []any{
		id, data.Login, data.Password, data.Role,
	}

	_, err := r.client.Exec(query, args...)
	if err != nil {
		return uuid.Nil(), err
	}

	return id, nil
}

func (r *PostgresUserRepository) GetToken(id uuid.UUID) (domain.Token, error) {
	query := `
		SELECT token FROM users
		WHERE id = $1
	`

	var token domain.Token
	if err := r.client.Get(&token, query, id); err != nil {
		if err == sql.ErrNoRows {
			return "", errors.ErrNotFound.WithMessage(fmt.Sprintf("user token not found: %s", id))
		}

		return "", err
	}

	return token, nil
}

func (r *PostgresUserRepository) SetToken(id uuid.UUID, token domain.Token) error {
	query := `
		UPDATE users 
		SET token = $1
		WHERE id = $2
	`

	args := []any{
		token, id,
	}

	_, err := r.client.Exec(query, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *PostgresUserRepository) DeleteToken(id uuid.UUID) error {
	query := `
		UPDATE users 
		SET token = $1
		WHERE id = $2
	`

	_, err := r.client.Exec(query, "", id.String())
	if err != nil {
		return err
	}

	return nil
}
