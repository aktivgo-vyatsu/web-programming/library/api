package repository

import (
	"github.com/semichkin-gopkg/uuid"
	"main/internal/domain"
)

type UserRepository interface {
	Get(uuid.UUID) (domain.User, error)
	GetByLogin(string) (domain.User, error)
	GetByLoginData(domain.UserLoginData) (domain.User, error)
	Save(domain.UserRegisterData) (uuid.UUID, error)
	GetToken(uuid.UUID) (domain.Token, error)
	SetToken(uuid.UUID, domain.Token) error
	DeleteToken(uuid.UUID) error
}
