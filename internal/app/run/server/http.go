package server

import (
	"fmt"
	httpController "main/internal/adapter/controller/http"
	"main/internal/adapter/middleware"
	"main/internal/app/config"
	"main/pkg/logging"
	"net/http"
)

func CreateHttpServer(
	logger logging.Logger,
	config config.Config,
	authMiddleware middleware.AuthMiddleware,
	userHttpController httpController.UserController,
	bookHttpController httpController.BookController,
) {
	router := configureRouter(
		authMiddleware,
		userHttpController,
		bookHttpController,
	)

	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", config.HttpServerPort),
		Handler: router,
	}

	logger.Info(0, fmt.Sprintf("http server started on port %d", config.HttpServerPort), nil)
	if err := server.ListenAndServe(); err != nil {
		logger.Warn(1, fmt.Sprintf("http server shutdown: %s", err.Error()), nil)
	}
}
