package server

import (
	"github.com/gorilla/mux"
	httpController "main/internal/adapter/controller/http"
	"main/internal/adapter/middleware"
	"net/http"
)

func configureRouter(
	authMiddleware middleware.AuthMiddleware,
	userHttpController httpController.UserController,
	bookHttpController httpController.BookController,
) *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/auth/signin", userHttpController.SignIn).Methods("POST")

	router.HandleFunc("/auth/signup", userHttpController.SignUp).Methods("POST")

	router.Handle("/auth/me",
		authMiddleware.Authentication(
			http.HandlerFunc(userHttpController.GetMe),
		),
	).Methods("GET")

	router.Handle("/auth/signout",
		authMiddleware.Authentication(
			http.HandlerFunc(userHttpController.SignOut),
		),
	).Methods("GET")

	router.Handle("/books/{id}",
		authMiddleware.Authentication(
			authMiddleware.AdminAuthorization(
				http.HandlerFunc(bookHttpController.Get),
			),
		),
	).Methods("GET")

	router.HandleFunc("/books", bookHttpController.GetList).Methods("GET")

	router.Handle("/books",
		authMiddleware.Authentication(
			authMiddleware.AdminAuthorization(
				http.HandlerFunc(bookHttpController.Create),
			),
		),
	).Methods("POST")

	router.Handle("/books/{id}",
		authMiddleware.Authentication(
			authMiddleware.AdminAuthorization(
				http.HandlerFunc(bookHttpController.Update),
			),
		),
	).Methods("PATCH")

	router.Handle("/books/{id}",
		authMiddleware.Authentication(
			authMiddleware.AdminAuthorization(
				http.HandlerFunc(bookHttpController.Delete),
			),
		),
	).Methods("DELETE")

	return router
}
