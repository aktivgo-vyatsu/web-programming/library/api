package mock

import (
	"gitlab.collabox.dev/go/logging"
	bookHttpController "main/internal/adapter/controller/http/book"
	userHttpController "main/internal/adapter/controller/http/user"
	"main/internal/adapter/middleware/auth"
	bookRepository "main/internal/adapter/repository/book"
	userRepository "main/internal/adapter/repository/user"
	"main/internal/app/config"
	"main/internal/app/run/server"
	bookModel "main/internal/domain/book/model"
	tokenService "main/internal/domain/token/service"
	userModel "main/internal/domain/user/model"
)

func Run(config config.Config) {
	logger := logging.New()

	jwtTokenService := tokenService.NewJwtTokenService(config.TokenSigningKey)

	memoryUserRepository := userRepository.NewMemoryUserRepository()
	userModel := userModel.NewUserModel(memoryUserRepository, jwtTokenService)
	userController := userHttpController.NewHttpUserController(logger, userModel)

	memoryBookRepository := bookRepository.NewMemoryBookRepository()
	bookModel := bookModel.NewBookModel(memoryBookRepository)
	bookController := bookHttpController.NewHttpBookController(
		logger,
		userModel,
		bookModel,
	)

	authMiddleware := auth.NewAuthMiddleware(logger, jwtTokenService)

	server.CreateHttpServer(
		logger,
		config,
		authMiddleware,
		userController,
		bookController,
	)
}
