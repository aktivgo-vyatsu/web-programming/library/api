package dev

import (
	"fmt"
	"main/infra/storage/postgres"
	bookHttpController "main/internal/adapter/controller/http/book"
	userHttpController "main/internal/adapter/controller/http/user"
	"main/internal/adapter/middleware/auth"
	bookRepository "main/internal/adapter/repository/book"
	userRepository "main/internal/adapter/repository/user"
	"main/internal/app/config"
	"main/internal/app/run/server"
	bookModel "main/internal/domain/book/model"
	tokenService "main/internal/domain/token/service"
	userModel "main/internal/domain/user/model"

	"gitlab.collabox.dev/go/logging"
)

func Run(config config.Config) {
	logger := logging.New()

	postgresConnection := postgres.CreateConnection(config.LibraryDBCockroachConnection)
	defer func() {
		err := postgresConnection.Close()
		if err != nil {
			logger.Fatal(1, fmt.Sprintf("postgres connection close failed: %s", err.Error()), nil)
		}

		logger.Info(0, "postgres conn closed", nil)
	}()

	jwtTokenService := tokenService.NewJwtTokenService(config.TokenSigningKey)

	postgresUserRepository := userRepository.NewPostgresUserRepository(postgresConnection)
	userModel := userModel.NewUserModel(postgresUserRepository, jwtTokenService)
	userController := userHttpController.NewHttpUserController(logger, userModel)

	postgresBookRepository := bookRepository.NewPostgresBookRepository(postgresConnection)
	bookModel := bookModel.NewBookModel(postgresBookRepository)
	bookController := bookHttpController.NewHttpBookController(
		logger,
		userModel,
		bookModel,
	)

	authMiddleware := auth.NewAuthMiddleware(logger, jwtTokenService)

	server.CreateHttpServer(
		logger,
		config,
		authMiddleware,
		userController,
		bookController,
	)
}
