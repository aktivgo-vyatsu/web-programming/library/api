package config

type Config struct {
	HttpServerPort               int    `env:"HTTP_SERVER_PORT,required" envDefault:"8080"`
	LibraryDBCockroachConnection string `env:"LIBRARY_DB_COCKROACH_CONNECTION,required" envDefault:"postgres://cockroach@library_cockroach:26257/library?sslmode=disable"`
	TokenSigningKey              string `env:"TOKEN_SIGNING_KEY" envDefault:"super-secret-key"`
}
