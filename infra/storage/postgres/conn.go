package postgres

import (
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"gitlab.collabox.dev/go/logging"
	"time"
)

var logger = logging.New()

func CreateConnection(connStr string) *sqlx.DB {
	for {
		db, err := tryCreateConnection(connStr)
		if err == nil {
			return db
		}

		logger.Error(1, "failed connect to postgres", map[string]any{
			"error":    err.Error(),
			"conn_str": connStr,
		})

		time.Sleep(time.Second)
	}
}

func tryCreateConnection(connStr string) (*sqlx.DB, error) {
	conn, err := sqlx.Connect("pgx", connStr)
	return conn, err
}
