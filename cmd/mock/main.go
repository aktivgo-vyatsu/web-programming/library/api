package main

import (
	"github.com/aktivgo-gopkg/unexpected"
	"main/internal/app/config"
	"main/internal/app/run/mock"
	helpers "main/pkg/helper"
)

func main() {
	mock.Run(unexpected.Must(helpers.FromEnv[config.Config]()))
}
