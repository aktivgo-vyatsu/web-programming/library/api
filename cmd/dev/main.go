package main

import (
	"github.com/aktivgo-gopkg/unexpected"
	"main/internal/app/config"
	"main/internal/app/run/dev"
	helpers "main/pkg/helper"
)

func main() {
	dev.Run(unexpected.Must(helpers.FromEnv[config.Config]()))
}
