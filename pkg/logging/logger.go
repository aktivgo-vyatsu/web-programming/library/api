package logging

type Logger interface {
	Debug(errorCode int, message string, detailed any)
	Info(errorCode int, message string, detailed any)
	Warn(errorCode int, message string, detailed any)
	Error(errorCode int, message string, detailed any)
	Panic(errorCode int, message string, detailed any)
}
