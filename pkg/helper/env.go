package helpers

import "github.com/caarlos0/env/v6"

func FromEnv[T any]() (T, error) {
	var config T
	err := env.Parse(&config)
	return config, err
}
